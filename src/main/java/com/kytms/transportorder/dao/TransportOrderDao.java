package com.kytms.transportorder.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2018-01-05
 */
public interface TransportOrderDao<Order> extends BaseDao<Order> {
}
