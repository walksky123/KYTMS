package com.kytms.orderabnormal.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 订单异常管理Dao
 *
 * @author 陈小龙
 * @create 2018-01-11
 */
public interface AbnormalDetailDao<AbnormalDetail> extends BaseDao<AbnormalDetail> {
}
