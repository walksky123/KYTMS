package com.kytms.shipmentTemplate.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.service.BaseService;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 运单模板SERVICE
 *
 * @author 陈小龙
 * @create 2018-04-09
 */
public interface TemplateService<ShipmentTemplate> extends BaseService<ShipmentTemplate> {
    JgGridListModel getList(CommModel commModel);
    ShipmentTemplate  saveTemplate(ShipmentTemplate shipmentTemplate);
}
